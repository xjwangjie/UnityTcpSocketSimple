﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Lifetime;
using System.Windows.Forms;
using Telepathy.Clients;
using Telepathy.Protocols;
using Telepathy.Servers;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using Button = UnityEngine.UI.Button;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public ScrollRect scrollRect;
    public GameObject messageTextPrefab;
    public GameObject texmsgPrefab;
    public Transform viewContent;

    public InputField fieldText;

    public Button sendImgBtn;
    public Button sendBtn;

    ClientManager clientManager;

    private void Awake()
    {
        Instance = this;
        clientManager = GetComponent<ClientManager>();
    }

    private void Start()
    {
        sendBtn.onClick.AddListener(OnSendMessage);
        sendImgBtn.onClick.AddListener(OnLoadImg);
    }

    void OnSendMessage()
    {
        try
        {
            string str = fieldText.text;
            if (string.IsNullOrEmpty(str)) return;

                clientManager.SendMsg(str);
            
            fieldText.text = string.Empty;
            scrollRect.verticalScrollbar.value = 0;

            GameObject textobj = Instantiate(messageTextPrefab, viewContent);
            Text text = textobj.GetComponent<Text>();
            text.text = string.Format("我: {0}", str);
        }
        catch (Exception)
        {

        }
    }

    public void OnLoadImg()
    {
        OpenFileDialog od = new OpenFileDialog();
        od.Title = "请选择头像图片";
        od.Multiselect = false;
        od.Filter = "图片文件(*.jpg,*.png,*.bmp)|*.jpg;*.png;*.bmp";
        if (od.ShowDialog() == DialogResult.OK)
        {
            Debug.LogWarning(od.FileName);
            StartCoroutine(GetTexture("file://" + od.FileName, (tex) =>
            {
                byte[] png = tex.EncodeToJPG(50);
                RectClass rect = new RectClass();
                rect.x = 0;
                rect.y = 0;
                rect.width = tex.width;
                rect.height = tex.height;
                string str = JsonConvert.SerializeObject(rect);

                clientManager.SendMsg(str, HeaderType.Content, png);

                GameObject textobj = Instantiate(texmsgPrefab, viewContent);
                RawImage img = textobj.GetComponent<RawImage>();
                img.texture = tex;
                scrollRect.verticalScrollbar.value = 0;
            }));

            od.Dispose();
        }
    }
    public void OnRecvMessage(string username,MessageBase myMsg)
    {

        byte[] bytes = myMsg.bytes;
        if (bytes.Length > 0)
        {
            GameObject textobj = Instantiate(texmsgPrefab, viewContent);
            RawImage img = textobj.GetComponent<RawImage>();
            RectClass rect = JsonConvert.DeserializeObject<RectClass>(myMsg.text);
            Texture2D tex = new Texture2D(rect.width, rect.height);
            tex.LoadImage(bytes);
            img.texture = tex;
        }
        else
        {
            GameObject textobj = Instantiate(messageTextPrefab, viewContent);
            Text text = textobj.GetComponent<Text>();
            text.text = string.Format("来自{0}消息: {1}", username, myMsg.text);
        }
        scrollRect.verticalScrollbar.value = 0;
    }



    IEnumerator GetTexture(string url, UnityAction<Texture2D> callback)
    {
        WWW www = new WWW(url);
        yield return www;
        if (www.isDone && www.error == null)
        {
            Texture2D img = www.texture;
            callback.Invoke(img);
        }
    }
}
