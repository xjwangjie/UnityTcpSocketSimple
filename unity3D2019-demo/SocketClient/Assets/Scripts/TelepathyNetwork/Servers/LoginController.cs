﻿using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Telepathy.Servers
{
    public class LoginController
    {
        ServerManager serverMgr;
        public LoginController(ServerManager serverMgr) {
            this.serverMgr = serverMgr;
        }

        #region 储存登录客户端
        /// <summary>
        /// 存储登录用户
        /// </summary>
        Dictionary<string, MySession> LoginSessionDict = new Dictionary<string, MySession>();


        /// <summary>
        /// 添加到登录列表
        /// </summary>
        public bool AddClient(MySession session)
        {
            LoginSessionDict[session.username] = session;
            return true;
        }

        /// <summary>
        /// 移除指定登录端
        /// </summary>
        /// <returns></returns>
        public bool RemoveClient(MySession session)
        {
            if (LoginSessionDict.ContainsKey(session.username))
            {
                return LoginSessionDict.Remove(session.username);
            }
            return false;
        }

        /// <summary>
        /// 获取所有端
        /// </summary>
        /// <returns></returns>
        public List<MySession> GetAllClient()
        {
            List<MySession> sessions = new List<MySession>();
            foreach (var client in LoginSessionDict)
            {
                sessions.Add(client.Value);
            }
            return sessions;
        }

        /// <summary>
        /// 获取除自身之外的 所有端
        /// </summary>
        /// <returns></returns>
        public List<MySession> GetAllClientExcBody(MySession self)
        {
            List<MySession> sessions = new List<MySession>();
            foreach (var client in LoginSessionDict)
            {
                if (client.Value == self) continue;
                sessions.Add(client.Value as MySession);
            }
            return sessions;
        }

        /// <summary>
        /// 获取指定客户端
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public MySession GetClient(string username)
        {
            if (LoginSessionDict.ContainsKey(username))
            {
                return LoginSessionDict[username];
            }
            return null;
        }

        /// <summary>
        /// 获取指定客户端
        /// </summary>
        /// <param name="client"></param>
        /// <returns></returns>
        public MySession GetClient(int clientId)
        {
            foreach (var item in LoginSessionDict)
            {
                if (item.Value.connId == clientId)
                {
                    return item.Value;
                }
            }
            return null;
        }
        /// <summary>
        /// 登录
        /// </summary>
        public bool AllLogin(MySession session, Telepathy.Protocols.LoginMessage loginMsg)
        {
            //无需验证 直接登录使用
            LoginClass login = JsonConvert.DeserializeObject<LoginClass>(loginMsg.MsgBase.text);
            session.userId = login.userid;
            session.username = login.username;
            session.password = login.password;
            Debug.Log(string.Format("用户名: {0} 登录操作! 在线用户:{1} ", session.username, GetAllClient().Count));
            return AddClient(session);
        }

        /// <summary>
        /// 退出登录, 连接断开直接就退出登录
        /// </summary>
        public bool UnLogin(int connId)
        {
            List<MySession> sessions = GetAllClient();
            for (int i = 0; i < sessions.Count; i++)
            {
                 MySession my = sessions[i];
                if (my.connId == connId)
                {
                    
                    Debug.Log(string.Format("用户:{0} 已断开!", my.username));
                    Debug.Log(string.Format("剩余登录数量: {0}", GetAllClient().Count));
                    return RemoveClient(my);
                }
            }
            Debug.LogError("找不到用户!");
            return false;
        }

        #endregion

    }
}