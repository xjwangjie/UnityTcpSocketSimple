﻿using UnityEngine.Networking;

namespace Telepathy.Servers
{
    public class MySession
    {
        public int connId { get; private set; }
        public MySession(int connId)
        {
            this.connId = connId;
        }

        string _nickname = string.Empty;
        public string nickName
        {
            get
            {
                if (string.IsNullOrEmpty(_nickname)) return username;
                return _nickname;
            }
            set { }
        }

        public long userId;
        public string username;
        public string password;
    }
}
