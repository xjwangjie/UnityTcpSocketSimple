# UnityTcpSocketSimple

#### Description

unity3D Tcp框架, 基于Telepathy , 包含服务端和客户端, 用于unity内端通讯,高效稳定.

Telepathy 是unity Mirror联机插件里面的应用的

Telepathy https://github.com/vis2k/Telepathy

#### Instructions
Client:

    public class ClientManager : BaseTcp
    {
        private void Start()
        {
            InvokeRepeating("ConnectedInterval", 0, connectInterval); //发送心跳
        }

        #region 其他消息接收
        protected override void OnConnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端启动! id:{0}", msg.connectionId));
        }

        protected override void OnDisconnected(Message msg)
        {
            Debug.LogWarning(string.Format("断开链接! id:{0}", msg.connectionId));
        }
        protected override void OnLoginMessage(int connID, Protocols.LoginMessage msg)
        {
            base.OnLoginMessage(connID, msg);
            Dictionary<string, string> keyValues = JsonConvert.DeserializeObject<Dictionary<string, string>>(msg.MsgBase.text);
            foreach (var item in keyValues)
            {
                Debug.LogWarning(string.Format("{0} - {1}", item.Key, item.Value));
            }
        }

        protected override void OnHeardMessage(int connID, Protocols.HeartMessage msg)
        {
            base.OnHeardMessage(connID, msg);
            Debug.LogWarning("收到服务器返回的心跳消息:{0}"+msg.MsgBase.text);
        }
        #endregion

        protected override void OnRecvMessage(int connID, Telepathy.Protocols.MyMessage msg)
        {
            Debug.LogWarning(string.Format("收到来自 {0} 服务端消息:{1}", connID, msg.MsgBase.text));

        }

        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="headerType"></param>
        /// <returns></returns>
        public bool SendMsg(string strMsg, HeaderType headerType = HeaderType.Content, byte[] bytes = null)
        {
            MessageBase myMsg = GetMessageBase(strMsg, bytes, headerType);
            myMsg.messageId = MsgStringId;
            return SendToserver(myMsg.Serialize());
        }

        #region 心跳
        /// <summary>
        /// 设置重连次数
        /// </summary>
        [Space(10)] public int reConnCount = 1000;
        /// <summary>
        /// 链接间隔时间
        /// </summary>
        float connectInterval = 1f;

        /// <summary>
        /// 链接次数
        /// </summary>
        int connectCount = 0;

        public UnityAction<int> OnConnInterval = null;

        /// <summary>
        /// 心跳检查 断开重连 在Update中调用才有效
        /// </summary>
        protected void ConnectedInterval()
        {
            if (client == null || client.Connected == false)
            {
                Destroy();
                if (connectCount < reConnCount)
                {
                    //链接次数增加
                    connectCount++;
                    string str = string.Format("这是第{0}次 连接", connectCount);
                    Debug.Log(str);
                    StartClient(this.Ip, this.Port);  //重连一次
					if(client!=null) client.MaxMessageSize = 1024 * 1024;
                }
                OnConnInterval?.Invoke(connectCount);
            }
            else if (client.Connected)
            {
                SendMsg("1", HeaderType.Heart);
            }
        }
        #endregion
    }


Server:

    public class ServerManager : BaseTcp
    {

        protected override void Awake()
        {
            base.Awake();
            loginManager = new LoginController(this);
        }

        private void Start()
        {
            StartServer(this.Port);
            server.MaxMessageSize = 1024 * 1024;
        }

        protected override void OnConnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端连接! id:{0}", msg.connectionId));
        }

        protected override void OnDisconnected(Message msg)
        {
            Debug.LogWarning(string.Format("客户端断开! id:{0}", msg.connectionId));
        }

        protected override void OnRecvMessage(int connID, Telepathy.Protocols.MyMessage msg)
        {
            Debug.LogWarning(string.Format("收到来自 {0} 客户端消息, 类型:{1} 内容:{2}", connID, msg.MsgBase.headerType, msg.MsgBase.text));
        }

        protected override void OnLoginMessage(int connID, Protocols.LoginMessage msg)
        {
            base.OnLoginMessage(connID, msg);
			Debug.LogWarning(string.Format("客户端登录操作! id:{0}", msg.connectionId));
        }

        protected override void OnHeardMessage(int connID, Protocols.HeartMessage msg)
        {
            base.OnHeardMessage(connID, msg);
            Debug.LogWarning(string.Format("收到心跳消息:{0}", msg.MsgBase.text));
            SendMsg(connID, msg.MsgBase.text, HeaderType.Heart); //直接返回心跳消息
        }
        /// <summary>
        /// 发送消息
        /// </summary>
        /// <param name="strMsg"></param>
        /// <param name="headerType"></param>
        /// <returns></returns>
        public bool SendMsg(int clientID, string strMsg, HeaderType headerType = HeaderType.Content, byte[] bytes = null)
        {
            MessageBase myMsg = GetMessageBase(strMsg, bytes, headerType);
            myMsg.messageId = MsgStringId;
            return SendToClient(clientID, myMsg.Serialize());
        }

        /// <summary>
        /// 转发使用
        /// </summary>
        /// <param name="clientID"></param>
        /// <param name="myMsg"></param>
        /// <returns></returns>
        public bool RelaySend(int clientID, MessageBase myMsg)
        {
            return SendToClient(clientID, myMsg.Serialize());
        }
    }


#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
